using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zahlenauswertung_GUI
{
    public partial class Form1 : Form
    {
        // Setzen der globalen Variablen
        private double zahl, summe = 0, min, max, durchschnitt;
        private int anzahl = 0;

        public Form1()
        {
            InitializeComponent();
        }

        // Das Programm beenden
        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Eingabe- und Ausgabefeld löschen und den Coursor in das Eingabefeld für eine neue Engabe positionieren
        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_input.Text = txt_output.Text = null;
            anzahl = 0;
            summe = 0;
            txt_input.Focus();
        }

        private void cmd_output_Click(object sender, EventArgs e)
        {
            if(anzahl > 0)
            {
                durchschnitt = summe / anzahl;
                txt_output.Text = "Anzahl Eingaben: " + anzahl + Environment.NewLine;
                txt_output.Text += "Gesamtwert der Eingaben: " + summe.ToString("F2") + Environment.NewLine;
                txt_output.Text += "Durchschnittswert der Eingaben: " + durchschnitt.ToString("F2") + Environment.NewLine;
                txt_output.Text += "Kleinste Eingabe: " + min.ToString("F2") + Environment.NewLine;
                txt_output.Text += "Größte Eingabe: " + max.ToString("F2") + Environment.NewLine;
            }
            else
            {
                MessageBox.Show("Division durch 0 nicht definiert");
                txt_input.Clear();
                txt_input.Focus();
            }
        }

        private void cmd_input_Click(object sender, EventArgs e)
        {
            try
            {
                zahl = Convert.ToDouble(txt_input.Text);

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                txt_input.Clear();
                txt_output.Focus();
            }
            finally
            {
                if (anzahl == 0)
                    min = max = zahl;
                anzahl++;
                summe += zahl;
                if (zahl < min)
                    min = zahl;
                if (zahl > max)
                    max = zahl;
                txt_input.Clear();
                txt_input.Focus();
            }
        }
    }
}